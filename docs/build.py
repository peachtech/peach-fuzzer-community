#!/usr/bin/python

#
# Run asciidoc against all source files to build website
#

import os, sys, subprocess

# when filenames are provided the build script will skip regenerating anything
# that isn't in the provided list of files
SPECIFIC_FILES = []
if len(sys.argv) > 1:
    SPECIFIC_FILES = sys.argv[1:]
    # prevent funny business trying to match leading directory dot
    SPECIFIC_FILES = map(lambda s: s.lstrip('./'), SPECIFIC_FILES)
    print("Only regenerating the following file names: %s" % SPECIFIC_FILES)

pagesBase_community = '.'
out_community = '../public'

os.system("rm -rf "+out_community)

os.system("mkdir -p "+out_community+"/images")
os.system("cp images/* "+out_community+"/images")

def generate_site_section(pagesbase, out_location):
    for root, subFolders, files in os.walk(pagesbase, topdown=False):

            outFolder = root.replace(pagesbase, out_location)

            try:
                    os.makedirs(outFolder)
            except:
                    pass

            cmd = "cp css/* "+outFolder+"/."
            os.system(cmd)

            for folder in subFolders:
                    folder = os.path.join(outFolder, folder)
                    print("mkdir " + folder)

                    try:
                            os.makedirs(folder)
                    except:
                            pass

            outFolder = root.replace(pagesbase, out_location)
            for file in files:
                    if not file.endswith(".adoc"):
                      continue

                    pageWithPath = os.path.join(root, file)
                    matches = [sf for sf in SPECIFIC_FILES if pageWithPath.endswith(sf)]
                    if SPECIFIC_FILES and not matches:
                        continue
                    newOut = os.path.join(outFolder, file).replace(".adoc", ".html")

                    cmd = "asciidoc --backend=xhtml11 --conf-file=website.conf -o " + newOut + " " + pageWithPath
                    print(cmd)
                    os.system(cmd)

generate_site_section(pagesBase_community, out_community)

# end
